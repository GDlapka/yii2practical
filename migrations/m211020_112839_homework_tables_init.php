<?php

use yii\db\Migration;

/**
 * Class m211020_112839_homework_tables_init
 * Миграция создания таблиц тестового задания
 */
class m211020_112839_homework_tables_init extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%note}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string(30)->notNull(),
            'email' => $this->string(50)->notNull(),
            'homepage' => $this->string(256)->null(),
            'message' => $this->text()->notNull(),
            'file' => $this->char(69)->null(),
            'ip' => $this->char(15)->null(),
            'browser' => $this->string()->null(),
        ]);

        $this->createTable('{{%user}}', [
            'username' => $this->string(256)->notNull()->unique(),
            'password' => $this->char(60)->notNull(),
            'email' => $this->string(100)->notNull(),
            'homepage' => $this->string(256)->null(),
            'role' => 'ENUM(\'admin\', \'user\') DEFAULT \'user\' NOT NULL',
        ]);

        /**
         * Добавляем УЗ admin|admin
         */
        $this->insert('user', [
            'username' => 'admin',
            'password' => '$2y$10$HL0cDbQD8RPngVxuFKmCa.KcyINUGaLULZ72q6yEqx8xh3rUTf9L2',
            'email' => 'admin@admin.ru',
            'role' => 'admin',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%note}}');
        $this->dropTable('{{%user}}');
    }
}
