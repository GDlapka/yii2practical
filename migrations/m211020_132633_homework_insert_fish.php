<?php

use yii\db\Migration;

/**
 * Class m211020_132633_homework_insert_fish
 * Добавляем рыбу в notes
 */
class m211020_132633_homework_insert_fish extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $notes = [];
        $notes[] = ['admin', 'admin@admin.ru', 'Hi there! Здравствуйте, Добро пожаловать в нашу гостевую. ' .
            'Просим Вас соблюдать некоторые правила: 
            1) В Гостевой книге не допускается использование не нормативной лексики, брани, клеветы в адрес учреждения ' .
            'и других лиц, в том числе слов при подмене букв символами в адрес других посетителей, администрации или ' .
            'любого другого лица или учреждения, пропаганда любых форм национальной, религиозной, расистcкой ненависти ' .
            'или насилия; 
            2)Вся переписка по причинам удаления сообщений и другим подобным вопросам ведется исключительно по ' .
            'E-mail. 
            3) Настоятельно просим Вас указывать настоящий email при отправке сообщения без регистрации, это является ' .
            'необходимым для разрешения вопросов удаления сообщений. 
            Спасибо за внимание!'];

        for ($i = 0; $i < 250; ++$i) {
            $notes[] = ['admin', 'admin@admin.ru', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do 
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud 
            exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in 
            reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat 
            cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. '];
        }

        $this->batchInsert('note', ['username', 'email', 'message'], $notes);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "Please run m211020_112839_homework_tables_init.php to drop tables.\n";
    }
}
