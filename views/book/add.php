<?php

/* @var $this yii\web\View */
/* @var $model app\models\Note  */
/* @var $uploader app\models\Upload  */

use yii\captcha\Captcha;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Новое сообщение';
?>
<div class="site-about">
    <?php
    $form = ActiveForm::begin([
        'id' => 'login-form',
        'options' => ['class' => 'form-horizontal'],
    ]) ?>

    <?= $form->field($model, 'username', ['inputOptions' => ['autofocus' => 'autofocus']])->
            textInput()?>

    <?= $form->field($model, 'email')->textInput()?>

    <?= $form->field($model, 'homepage')->textInput()?>

    <?= $form->field($model, 'message')->textarea(['rows' => 6])?>

    <?= $form->field($uploader, 'file')->fileInput()?>

    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton('Отправить сообщение', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end() ?>
</div>
