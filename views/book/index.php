<?php

/* @var $this yii\web\View */
/* @var $model app\models\Note */

use yii\grid\GridView;

$this->title = 'Yii2 - Гостевая книга';

$this->registerJsFile('@web/js/note.js', ['depends' => yii\web\JqueryAsset::class]);

$grid = GridView::widget([
    'dataProvider' => $model->provider,
    'columns' => [
        'id',
        [
            'label' => 'user',
            'attribute' => 'username',
        ],
        'email',
        'homepage',
        'message',
    ],
    'summary' => 'страницы {page} из {pageCount}',
    'pager' => [
        'maxButtonCount' => 10,
        'nextPageLabel' => '>',
        'prevPageLabel' => '<',
        'firstPageLabel' => '<<<',
        'lastPageLabel'  => '>>>',
        'options' => [
            'class' => 'pager-wrapper',
        ],
    ],
]);
?>

<div id="message-preview" >
    <div class="message-box">
        <h3><b>UserName</b> email@email.com</h3>
        <p>
            HELLO! This is Lorem.
        </p>
    </div>
</div>

<div class="site-index">
    <div id="notesGrid">
        <?=$grid?>
    </div>
</div>
