<?php

namespace app\models;

use yii\base\Model;

class Upload extends Model
{
    public $file;
    private $name;

    public function getName()
    {
        return $this->name;
    }

    public function rules()
    {
        return [
            [
                'file',
                'file',
                'extensions' => 'txt, png, jpg, jpeg',
                'maxSize' => '102400',
                'tooBig' => 'Загружаемый файл слишком большой, предел - 100Кб',
            ]
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            if (!$this->file) {
                return true;
            }
            $name = hash_file('sha256', $this->file->tempName)  . '.' . $this->file->extension;
            $path = \Yii::getAlias('@webroot');
            $path .= '/upload/';
            $path .= $name;
            $this->name = $name;
            $this->file->saveAs($path);
            return true;
        } else {
            return false;
        }
    }

}