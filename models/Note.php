<?php

namespace app\models;

use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "note".
 *
 * @property int $id
 * @property string $username
 * @property string $email
 * @property string|null $homepage
 * @property string $message
 * @property string|null $file
 */
class Note extends \yii\db\ActiveRecord
{

    public $provider;
    public $pagSize = 10;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'note';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'email', 'message'], 'required'],
            [['message'], 'string'],
            [['username'], 'string', 'max' => 30],
            [['email'], 'string'],
            [['homepage'], 'string'],
            [['file'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'username' => 'Nickname',
            'email' => 'e-mail',
            'homepage' => 'Домашняя страница',
            'message' => 'Сообщение',
            'file' => 'Прикрепить файл',
        ];
    }

    public function getNotes()
    {
        $this->provider = new ActiveDataProvider([
            'query' => Note::find(),
            'pagination' => [
                'pageSize' => $this->pagSize,
                'pageSizeLimit' => [1, 50],
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_ASC,
                ],
                'attributes' => [
                    'id',
                    'username',
                    'email',
                ],
            ],
        ]);
    }
}
