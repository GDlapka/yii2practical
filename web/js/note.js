$(document).ready(function () {
   $(document).mouseup(function (e) {
      let div = $('#message-preview');
      let messageBox = div.children('.message-box');
      if (messageBox.has(e.target).length == 0) {
         if (div.css('visibility') == 'visible') {
            div.css('visibility', 'hidden');
         } else {
            if (e.target.tagName == 'TD') {
               let id = $(e.target).parent().children()[0].innerHTML;
               $.ajax({
                  url: '/get-note',
                  method: 'get',
                  dataType: 'json',
                  data: {id},
                  success: function (data) {
                     messageBox.html(
                         '<h3><b>' + data.username +
                         '</b> ' + data.email + '</h3>' +
                         '<p>' + data.message +

                         '</p>'
                     );
                     div.css('visibility', 'visible');
                  },
               });
            }
         }
      }
   });
});